package com.FSD.Phase2.Project.FlyAwayProject1;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FSD.Pase2.Project.FlyAway.DAO.FlightDAO;
import com.FSD.Phase2.Project.FlyAway.Entity.Flight;

@Path("flight")
public class FlightResources {

	@GET
	@Path("details")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Flight> getFlight() {
		FlightDAO dao = new FlightDAO();
		List<Flight> flight = dao.getFlight();
		return flight;
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addFlight(Flight flight) {

		FlightDAO dao = new FlightDAO();
		dao.addFlight(flight);

		return Response.ok().build();
	}

	@PUT
	@Path("/update/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateFlight(@PathParam("id") int id, Flight flight) {
		FlightDAO dao = new FlightDAO();
		int count = dao.updateFlight(id, flight);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteFlight(@PathParam("id") String id) {
		FlightDAO dao = new FlightDAO();
		int count = dao.deleteFlight(id);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}

}
