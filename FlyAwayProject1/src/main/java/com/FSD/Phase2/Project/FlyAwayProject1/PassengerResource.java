package com.FSD.Phase2.Project.FlyAwayProject1;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FSD.Pase2.Project.FlyAway.DAO.FlightBookingDAO;
import com.FSD.Pase2.Project.FlyAway.DAO.PassangerDAO;
import com.FSD.Phase2.Project.FlyAway.Entity.FlightBookings;
import com.FSD.Phase2.Project.FlyAway.Entity.Passenger;

@Path("passanger")
public class PassengerResource {

	@GET
	@Path("details")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Passenger> getPassenger() {
		System.out.println("Connection Success");
		PassangerDAO pdao = new PassangerDAO();
		List<Passenger> Passengers = pdao.getPassengers();
		return Passengers;
	}

	
	  @POST
	  @Path ("/login/{emailId}")
	  @Consumes(MediaType.APPLICATION_JSON)
	  public String Login(@PathParam("emailId") String emailId, Passenger pass) {
		  PassangerDAO dao = new PassangerDAO();
		  String str= "";
		    boolean response = dao.getPassengerByEmailId(emailId,pass);
		   
		    if(response==true)
		    {
		    
		    		 str="Login Successful";
		    		 return str;
		    	 }
		    	 
		    
		    else
		    {
		    	str="Invalid Email or Passwords";
		    	return str;
		    	
		    }
		    
		    	
		    }
	       
		   
		
	/*
	 * public String loginPassenger(Passenger Passenger) { String resp = "" ;
	 * PassangerDAO dao = new PassangerDAO();
	 * 
	 * Passenger loadedPassenger = dao.getPassengerByEmailId(Passenger); if(
	 * loadedPassenger!= null) {
	 * 
	 * if(loadedPassenger.getPassword().equals(Passenger.getPassword())) { resp=
	 * "Login Successfull"; // System.out.println("Logged n successfull"); } else
	 * resp= "Login UnSuccessfull"; }
	 * 
	 * 
	 * 
	 * else {
	 * 
	 * resp ="Account Id DOesn't Exists"; }
	 */
	  
	  
	  //return Response.ok().build(); 
	//  return resp;
	  
	  //}
	 

	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addPassengers(Passenger passenger) {

		PassangerDAO pdao = new PassangerDAO();
		pdao.addPassengers(passenger);
		return Response.ok().build();
	}

	@PUT
	@Path("/update/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePassengers(@PathParam("id") int id, Passenger pass) {
		PassangerDAO pdao = new PassangerDAO();
		int count = pdao.updatePassenger(id, pass);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deletePassenger(@PathParam("id") int id) {
		PassangerDAO pdao = new PassangerDAO();
		int count = pdao.deletePassenger(id);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}
}
