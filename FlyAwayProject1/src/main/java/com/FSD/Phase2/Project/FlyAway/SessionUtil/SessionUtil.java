package com.FSD.Phase2.Project.FlyAway.SessionUtil;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.FSD.Phase2.Project.FlyAway.Entity.Airport;
import com.FSD.Phase2.Project.FlyAway.Entity.Flight;
import com.FSD.Phase2.Project.FlyAway.Entity.FlightBookings;
import com.FSD.Phase2.Project.FlyAway.Entity.Passenger;


public class SessionUtil {
	
	
private static SessionUtil instance = new SessionUtil();
	
    private static SessionFactory sessionFactory;
    
    public static SessionUtil getInstance() {
    	return instance;
    }
    
    private SessionUtil() {
    	Configuration configuration = new Configuration();
    	configuration.configure("hibernate.cfg.xml")
    	.addAnnotatedClass(Airport.class)
    	.addAnnotatedClass(Flight.class)
    	.addAnnotatedClass(Passenger.class)
    	.addAnnotatedClass(FlightBookings.class);
    	sessionFactory = configuration.buildSessionFactory();    	
    }
    
    public static Session getSession() {
    	Session session =getInstance().sessionFactory.openSession();
    	return session;
    }

}
