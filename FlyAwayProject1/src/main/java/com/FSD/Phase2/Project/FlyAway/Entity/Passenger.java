package com.FSD.Phase2.Project.FlyAway.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="Passenger",uniqueConstraints=@UniqueConstraint(columnNames="Passanger_EmailId"))
public class Passenger {
	
	@Id
	@Column(name="Passanger_Number")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int passnumber;
	
	@Column(name="Passanger_Name")
	private String name;
	
	@Column(name="Passanger_Address")
	private String Address;
	
	@Column(name="Passanger_EmailId")
	private String emailId;
	
	@Column(name="Password")
	private String password;

	public Passenger() {
		super();
		
	}

	public Passenger(String name, String address, String emailId,String password) {
		super();
		this.name = name;
		Address = address;
		this.emailId = emailId;
		this.password=password;
	}

	public int getPassnumber() {
		return passnumber;
	}

	public void setPassnumber(int passnumber) {
		this.passnumber = passnumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
		
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	@Override
	public String toString() {
		return "Passenger [passnumber=" + passnumber + ", name=" + name + ", Address=" + Address + ", emailId="
				+ emailId + "]";
	}
	
	
	
	
	
	

}
