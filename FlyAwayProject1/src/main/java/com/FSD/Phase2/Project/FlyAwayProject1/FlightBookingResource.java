package com.FSD.Phase2.Project.FlyAwayProject1;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FSD.Pase2.Project.FlyAway.DAO.FlightBookingDAO;
import com.FSD.Pase2.Project.FlyAway.DAO.FlyAwayDAO;
import com.FSD.Phase2.Project.FlyAway.Entity.Airport;
import com.FSD.Phase2.Project.FlyAway.Entity.FlightBookings;

@Path("flightbooking")
public class FlightBookingResource {

	@GET
	@Path("details")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FlightBookings> getFlightBookings() {

		 FlightBookingDAO fbdao = new  FlightBookingDAO();
		List<FlightBookings> FlightBookings = fbdao.getFlightBookings();
		return FlightBookings;
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addFlightBookings(FlightBookings flightBookings) {

		 FlightBookingDAO fbd = new FlightBookingDAO();
		fbd.addFlightBookings(flightBookings);

		return Response.ok().build();
	}

	@PUT
	@Path("/update/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateFlightBookings(@PathParam("id") int id, FlightBookings flbooks) {
		FlightBookingDAO fbd = new FlightBookingDAO();
		int count = fbd.updateFlightBookings(id, flbooks);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}

@DELETE
@Path("/delete/{id}")
@Consumes(MediaType.APPLICATION_JSON)
public Response deleteFlightBookings(@PathParam("id") int id) {
	FlightBookingDAO fbd = new FlightBookingDAO();
	int count = fbd.deleteFlightBookings(id);
	if (count == 0) {
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	return Response.ok().build();
}
}
