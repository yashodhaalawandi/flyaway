package com.FSD.Phase2.Project.FlyAwayProject1;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.FSD.Pase2.Project.FlyAway.DAO.FlyAwayDAO;
import com.FSD.Phase2.Project.FlyAway.Entity.Airport;

@Path("airport")
public class FlyawayResource {

	
	@GET
	@Path("details")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Airport> getAirport() {
		
		FlyAwayDAO dao = new FlyAwayDAO();
		List<Airport> Airports = dao.getAirport();
		return Airports;
	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addAirport(Airport airport) {

		FlyAwayDAO dao = new FlyAwayDAO();
		dao.addAirport(airport);

		return Response.ok().build();
	}

	@PUT
	@Path("/update/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAirport(@PathParam("id") int id, Airport airport) {
		FlyAwayDAO dao = new FlyAwayDAO();
		int count = dao.updateAirport(id, airport);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteAirport(@PathParam("id") int id) {
		FlyAwayDAO dao = new FlyAwayDAO();
		int count = dao.deleteAirport(id);
		if (count == 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		return Response.ok().build();
	}

}
