package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.FSD.Phase2.Project.FlyAway.Entity.Airport;
import com.FSD.Phase2.Project.FlyAway.Entity.Flight;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;

public class FlightDAO {
	
	public void addFlight(Flight bean){
		Session session = SessionUtil.getSession();        
	    Transaction tx = session.beginTransaction();
	    addFlight(session,bean);        
        tx.commit();
        session.close();
        
    }
	
	public void addFlight(Session session, Flight bean)
	{
		Flight f=new Flight();
		f.setArrival(bean.getArrival());
		f.setArrivalDate(bean.getArrivalDate());
		f.setDeparture(bean.getDeparture());
		f.setDepartureDate(bean.getDepartureDate());
		f.setFlightcost(bean.getFlightcost());
		f.setNumber(bean.getNumber());
		session.save(f);
		
	}
   
	public List<Flight> getFlight()
	
	{
		Session session = SessionUtil.getSession();
		Query query=session.createQuery("from Flight");
		List<Flight> flightlist=query.list();
		session.close();
		return flightlist;
		
	}
	
	public int updateFlight(int id,Flight bean)
	{
		Session session = SessionUtil.getSession();
        Transaction tx = session.beginTransaction();
		String hql="update Flight set Flight_Cost=:Flight_Cost where id = :id";
		Query query=session.createQuery(hql);
		query.setString("id",bean.getNumber());
        query.setString("Flight_Cost",bean.getFlightcost());
		int result=query.executeUpdate();
		System.out.println("number of records updated " +result);
		tx.commit();
        session.close();
		return result;
	}
	

	    public int deleteFlight(String id) 
	    {
	        Session session = SessionUtil.getSession();
	        Transaction tx = session.beginTransaction();
	        String hql = "delete from Flight where id = :id";
	        //Flight bean=new Flight();
	        Query query = session.createQuery(hql);
	        query.setString("id",id);
	        int rowCount = query.executeUpdate();
	        System.out.println("Rows affected: " + rowCount);
	        tx.commit();
	        session.close();
	        return rowCount;
	    }
	

}
