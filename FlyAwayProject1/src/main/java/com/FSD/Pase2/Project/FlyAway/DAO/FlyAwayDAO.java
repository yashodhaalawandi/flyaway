package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.FSD.Phase2.Project.FlyAway.Entity.Airport;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;


public class FlyAwayDAO {
	
	
	public void addAirport(Airport bean){
		Session session = SessionUtil.getSession();        
	    Transaction tx = session.beginTransaction();
        addAirport(session,bean);        
        tx.commit();
        session.close();
        
    }
	
	public void addAirport(Session session, Airport bean)
	{
		Airport a=new Airport();
		a.setAirportcode(bean.getAirportcode());
		a.setAirportname(bean.getAirportname());
		session.save(a);
		
	}
   
	public List<Airport> getAirport()
	
	{
		Session session = SessionUtil.getSession();
		Query query=session.createQuery("from Airport");
		List<Airport> lisair=query.list();
		session.close();
		return lisair;
		
	}
	
	public int updateAirport(int id,Airport airport)
	{
		
		if(id<=0)
			return 0;
		  Session session = SessionUtil.getSession();
          Transaction tx = session.beginTransaction();
		String hql="update Airport set airport_name = :airport_name, airport_code=:airport_code where id = :id";
		Query query=session.createQuery(hql);
		query.setInteger("id",id);
        query.setString("airport_name",airport.getAirportname());
        query.setString("airport_code",airport.getAirportcode());
		int result=query.executeUpdate();
		System.out.println("number of records updated " +result);
		tx.commit();
        session.close();
		return result;
	}
	

	    public int deleteAirport(int id) {
	        Session session = SessionUtil.getSession();
	        Transaction tx = session.beginTransaction();
	        String hql = "delete from Airport where id = :id";
	        Query query = session.createQuery(hql);
	        query.setInteger("id",id);
	        int rowCount = query.executeUpdate();
	        System.out.println("Rows affected: " + rowCount);
	        tx.commit();
	        session.close();
	        return rowCount;
	    }
	
}