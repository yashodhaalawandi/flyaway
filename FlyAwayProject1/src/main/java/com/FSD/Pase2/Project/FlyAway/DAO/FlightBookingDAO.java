package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.FSD.Phase2.Project.FlyAway.Entity.Airport;
import com.FSD.Phase2.Project.FlyAway.Entity.FlightBookings;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;

public class FlightBookingDAO {
	
	public void addFlightBookings(FlightBookings bean)
	{
		Session session = SessionUtil.getSession();        
	    Transaction tx = session.beginTransaction();
	    addFlightBookings(session,bean);        
        tx.commit();
        session.close();
        
    }
	
	public void  addFlightBookings(Session session, FlightBookings bean)
	{
		FlightBookings fb=new  FlightBookings();
		fb.setCustomernumber(bean.getCustomernumber());
		fb.setNumber(bean.getNumber());
		session.save(fb);
		
	}
   
	public List<FlightBookings> getFlightBookings()
	
	{
		Session session = SessionUtil.getSession();
		Query query=session.createQuery("from FlightBookings");
		List<FlightBookings> fbs=query.list();
		session.close();
		return fbs;
		
	}
	
	public int updateFlightBookings(int id,FlightBookings bean)
	{
		
		if(id<=0)
			return 0;
		  Session session = SessionUtil.getSession();
          Transaction tx = session.beginTransaction();
		String hql="update FlightBookings set customernumber=:customer_number where id = :id";
		Query query=session.createQuery(hql);
		query.setInteger("id",id);
        query.setString("customer_number",bean.getCustomernumber());
        int result=query.executeUpdate();
		System.out.println("number of records updated " +result);
		tx.commit();
        session.close();
		return result;
	}
	

	    public int deleteFlightBookings(int id) {
	        Session session = SessionUtil.getSession();
	        Transaction tx = session.beginTransaction();
	        String hql = "delete from FlightBookings where id = :id";
	        Query query = session.createQuery(hql);
	        query.setInteger("id",id);
	        int rowCount = query.executeUpdate();
	        System.out.println("Rows affected: " + rowCount);
	        tx.commit();
	        session.close();
	        return rowCount;
	    }

}
