package com.FSD.Pase2.Project.FlyAway.DAO;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.FSD.Phase2.Project.FlyAway.Entity.Passenger;
import com.FSD.Phase2.Project.FlyAway.SessionUtil.SessionUtil;

public class PassangerDAO {
	
	private int count;
	
	public void addPassengers(Passenger bean){
		Session session = SessionUtil.getSession();        
	    Transaction tx = session.beginTransaction();
        addPassengers(session,bean);    
        tx.commit();
        session.close();
        
    }
	
	public void addPassengers(Session session, Passenger passenger)
	{
		Passenger pass=new Passenger();
		pass.setAddress(passenger.getAddress());
		pass.setEmailId(passenger.getEmailId());
		pass.setName(passenger.getName());
		pass.setPassnumber(passenger.getPassnumber());
		session.save(pass);
		
	}
   
	
	  public List<Passenger> getPassengers()
	  
	  { Session session = SessionUtil.getSession(); Query
	  query=session.createQuery("from Passenger");
	  List<Passenger> passengers=query.list(); 
	  session.close(); 
	  return passengers;
	  
	  }
	 
	
	
	
	
	/*
	 * public boolean getPassengerByEmailId(String emailid, Passenger bean) {
	 * Session session = SessionUtil.getSession(); String hql =
	 * "select p.emailId from Passenger p where p.emailId=:Passenger_EmailId"; Query
	 * query = session.createQuery(hql); query.setString("Passenger_EmailId",
	 * bean.getEmailId()); for (Iterator it = query.iterate(); it.hasNext();) {
	 * it.next(); count++; } System.out.println("Total rows: " + count); if (count
	 * == 1) { return true; } else { return false; } }
	 */ 
	
	  
	  
	  public boolean getPassengerByEmailId(String emailid, Passenger bean) {
	         Session session = SessionUtil.getSession();
	        String hql = "select p.emailId from Passenger p where p.emailId=:Passenger_EmailId and p.password=:Password" ;
	        Query query = session.createQuery(hql);
	        query.setString("Passenger_EmailId", bean.getEmailId());
	        query.setString("Password", bean.getPassword());
	        for (Iterator it = query.iterate(); it.hasNext();) {            
	            it.next();
	            count++;
	        }
	        System.out.println("Total rows: " + count);
	        if (count == 1) {
	            return true;
	        } else {
	            return false;
	        }
	    }   
	/*
	 * public List<Passenger> getPassengerByEmailId(Passenger passenger) { Session
	 * session=SessionUtil.getSession(); Query query=session.
	 * createQuery("from Passenger p where p.emailId= :Passanger_EmailId");
	 * query.setString("Passanger_EmailId", passenger.getEmailId());
	 * List<Passenger>pass1= query.list(); return pass1;
	 * 
	 * }
	 */
	
	
	 
	
	
	/*
	 * public List< Passenger> getPassengerByEmailId(Passenger passenger) { Session
	 * session =SessionUtil.getSession(); Passenger pass=new Passenger(); String
	 * hql="from Passenger where emailId=:Passanger_EmailId"; Query
	 * query=session.createQuery(hql); List<Passenger> pass1= query.list();
	 * 
	 * 
	 * return pass1; }
	 */
	 
	
	
	/*
	 * public Passenger getPassengerByEmail(Passenger passenger) { Session session =
	 * SessionUtil.getSession(); Criteria crit
	 * =session.getSession().createCriteria(Passenger.class);
	 * crit.add(Restrictions.eq("emailId", passenger.getEmailId())); return
	 * (Passenger) crit.list().get(1);
	 * 
	 * }
	 */
	
	
	public int updatePassenger(int id,Passenger pass)
	{
		
		if(id<=0)
			return 0;
		  Session session = SessionUtil.getSession();
          Transaction tx = session.beginTransaction();
		String hql="update Passenger set name = :Passanger_Name, Address=:Passenger_Address where id = :id";
		Query query=session.createQuery(hql);
		query.setInteger("id",id);
        query.setString("Passanger_Name",pass.getName());
        query.setString("Passenger_Address",pass.getAddress());
		int result=query.executeUpdate();
		System.out.println("number of records updated " +result);
		tx.commit();
        session.close();
		return result;
	}
	

	    public int deletePassenger(int id) {
	        Session session = SessionUtil.getSession();
	        Transaction tx = session.beginTransaction();
	        String hql = "delete from Passenger where id = :id";
	        Query query = session.createQuery(hql);
	        query.setInteger("id",id);
	        int rowCount = query.executeUpdate();
	        System.out.println("Rows affected: " + rowCount);
	        tx.commit();
	        session.close();
	        return rowCount;
	    }

}
